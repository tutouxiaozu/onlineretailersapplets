import App from './App'
//引入封装的跳转页面
import goPage from '@/utils/utils'
import "./css/index.css";
import Vue from 'vue'
//全局挂载
Vue.prototype.$goPage = goPage

async function newCloud() {
	const n_clound = new wx.cloud.Cloud({
		resourceEnv: "min-gucof",
		traceUser: true
	});
	await n_clound.init();
	return n_clound;
}
Vue.prototype.$cloud = newCloud()
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
