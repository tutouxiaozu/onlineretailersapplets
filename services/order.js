import request from "../utils/http"
//取消订单
export const _Clone_Order = (orderNumber) => request.put(`/p/myOrder/cancel/${orderNumber}`)

//删除订单
export const _Delete_Order = async (id) => await request.delete(`/p/myOrder`,id)