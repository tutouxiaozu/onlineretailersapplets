//获取详情的数据
import request from "../utils/http";

//根据商品id获取该商品的详情数据

export const _get_detail = id =>request.get('/prod/prodInfo',{prodId:id})

//详情页加入购物车数据
export const add_detail = async (data)=> await request.post('/p/shopCart/changeItem',data)

//判断商品是否在收藏页面
export const addYes = async (data) => await request.get('/p/user/collection/isCollection',data)
//点击收藏
export const add_Sou = async (data) =>request.post('/p/user/collection/addOrCancel',data)

// 根据商品返回评论分页数据
export const pin_lun = async (data) => request.get('/prodComm/prodCommPageByProd',data)

//结算生成订单信息
export const Ding_dan= async (data) =>request.post('/p/order/confirm',data)
// export const add_detail = async (data)=>request.post('/p/shopCart/changeItem',data)
