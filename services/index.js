import request from "../utils/http";

//轮播图数据
export const swiper_img = async () => await request.get("/indexImgs");
//新品推荐数据
export const new_product=async (params)=>await request.get("/prod/lastedProdPage",params)
//每日疯抢数据
export const day_crazy=async (params)=>await request.get("/prod/moreBuyProdList",params)
//首页列表接口
export const getIndexList=async (params)=>await request.get("/prod/prodListByTagId",params)
//商品分组标签
export const foodClassify=async (params)=>await request.get("/prod/tag/prodTagList",params)
//公告指定信息
export const notice_list=async ()=>await request.get("/shop/notice/topNoticeList")
//公告列表信息
export const noticeList=async (params)=>await request.get("/shop/notice/noticeList",params)
//搜索页面热门搜索数据
export const hot_searchList=async (params)=>await request.get("/search/hotSearchByShopId",params)
//搜索页查看全局热搜
export const hot_allSearch=async (params)=>await request.get("/search/hotSearch",params)
//公告列表详情
export const notice_detail=async ({id})=>await request.get(`/shop/notice/info/${id}`)
//分页排序搜索商品
export const sort_searchList=async (params)=>await request.get("/search/searchProdPage",params)
