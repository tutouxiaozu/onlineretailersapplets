
import request from "../utils/http";

// 结算，生成订单信息
export const _getOrderInformation = async data => await request.post('/p/order/confirm', data);

// 根据订单号进行支付
export const _getOrderPlay = async data => await request.post('/p/order/pay', data);
// {
// 	"orderNumbers": "",订单号
// 	"payType": 0支付方式 (1:微信支付 2:支付宝)
// }

// 提交订单，返回支付流水号
export const _getOrderSubmit = async data => await request.post('/p/order/submit', data);
// {
// 	"orderShopParam": [
// 		{
// 			"remarks": "",
// 			"shopId": 0
// 		}
// 	]
// }
