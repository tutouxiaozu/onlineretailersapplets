

import request from "../utils/http";

// 获取购物车的数据
export const _getCartList = async (data) => await request.post('/p/shopCart/info', data);

// 获取购物车失效商品列表
export const _getInvalid = async data => await request.get('/p/shopCart/expiryProdList', data);

// 获取购物车商品数量
export const _getCartNum = async data => await request.get('/p/shopCart/prodCount', data);

// 获取选中购物项总计、选中的商品数量,参数为购物车id数组
export const _getTotal = async (data) => await request.post('/p/shopCart/totalPay', data);

// 清空用户购物车所有物品
export const _deleteAllEmpty = async (data) => await request.delete('/p/shopCart/deleteAll', data);

// 清空用户失效商品
export const _deleteInvalidEmpty = async (data) => await request.delete('/p/shopCart/totalPay', data);

// 删除单个或者多个购物车商品
export const _deleteUserCart = async (data) => await request.delete('/p/shopCart/deleteItem', data);

// 添加、修改用户购物车物品
export const _getCartChangeItem = async (data) => await request.post('/p/shopCart/changeItem', data);
