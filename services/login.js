
import request from "../utils/http";

// 根据code登录
//https://sign.jasonandjay.com/broadcast/room
export const _login = data => request.post('/login',data);

// 更新用户信息
export const save_user_info = data => request.put("/p/user/setUserInfo",data);
