import request from "../utils/http";

// 收藏列表数量
export const _Collect_Num = async () => await request.get("/p/user/collection/count");

// 收藏列表数据
export const _Get_Like = async () => await request.get("/p/user/collection/prods")

// // 获取地址管理列表
export const _Get_Address = async () => await request.get("/p/address/list");

//获取用户地址
export const _Get_AddrId = async (addrId) => await request.get(`/p/address/addrInfo/${addrId}`,)

// // 删除地址订单用户地址
export const _Del_Address = async (addrId) => await request.delete(`/p/address/deleteAddr/${addrId}`, )

// // 新增用户地址
export const _Add_Address = async (params) => await request.post("/p/address/addAddr", params);

// // 修改/设置用户地址
export const _Edit_Address = async (params) => await request.put(`/p/address/updateAddr`, params);

// 获取订单列表
export const _Get_Order = async (query) => request.get("/p/myOrder/myOrder", query)

// 获取省市区地址
export const _Get_Province = params => request.get("/p/area/listByPid", params)

// 默认地址选中
export const _Check_Address = async (addrId) => await request.put(`/p/address/defaultAddr/${addrId}`)