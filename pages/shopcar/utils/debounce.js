
// 防抖
export const debounce = function (fn, timeout) {
    let _time = null
    return function () {
        let _arg = arguments
        clearTimeout(_time)
        _time = setTimeout(() => {
            fn.apply(this, _arg)
        }, timeout)
    }
}

// 节流
export const throttling = function (fn, timeout) {
    let _playing = null
    return function () {
        let _arg = arguments
        if (_playing) {
            return
        }
        _playing = true
        setTimeout(() => {
            fn.apply(this, _arg)
            _playing = false
        }, timeout)
    }
}