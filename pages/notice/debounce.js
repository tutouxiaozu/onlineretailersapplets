//闭包防抖
function Debounce(fn, delay) {
  let timer = null;
  //定义一个内部函数
  const debounce = function () {
    if (timer) clearTimeout(timer);
    timer = setTimeOut(() => {
      fn();
    }, delay);
  };
  return debounce;
}
//防抖
function debounce(timeout, fn) {
  let _time = null;
  return function () {
    let _arg = arguments;
    clearTimeout(_time);
    _time = setTimeout(() => {
      fn.apply(this, _arg);
    }, timeout);
  };
}
//节流
function throttling(timeout, fn) {
  let _playing = null;
  return function () {
    let _arg = arguments;
    if (_playing) {
      return;
    }
    _playing = true;
    setTimeout(() => {
      fn.apply(this, _arg);
      _playing = false;
    }, timeout);
  };
}
setInterval;
//浅拷贝
function shallowCopy(object) {
  //只拷贝对象
  if (!object || typeof object !== "object") return;
  //根据object的类型判断是新建一个数组还是对象
  let newObject = Array.isArray(object) ? [] : {};
  //遍历object，并且判断是object的属性才拷贝
  for (let key in object) {
    if (object.hasOwnProperty(key)) {
      newObject[key] = object[key];
    }
  }
  return newObject;
}
//深拷贝
function deepCopy(object) {
  if (!object || typeof object !== "object") return;
  let newObject = Array.isArray(object) ? [] : {};
  for (let key in object) {
    if (object.hasOwnProperty(key)) {
      newObject[key] =
        typeof object[key] === "object" ? deepCopy(object[key]) : object[key];
    }
  }
  return newObject;
}
