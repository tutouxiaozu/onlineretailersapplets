
// 格式化 GET 请求路径
export const formetUrl = (url, query) => {
    /*
        https://api.juejin.cn/tag_api/v1/?aid=2608&spider=0
        {
            aid:"2608",
            spider:"0"
        }
    */
    return Object.keys(query).length > 0 ? url + '?' + Object.keys(query).map(item => `${item}=${query[item]}`).join('&') : url;
}

export default function({
	url = '',
	query = {},
	redirect = false,
	tabber = false
}) {
	// 跳转页面式 tabber swicthTab
	// redirect redirectTo
	// 默认使用 navigateTo
	url = formetUrl(url, query);
	const typeName = redirect ? "redirectTo" : tabber ? "switchTo" : 'navigateTo';

	return new Promise((resolve, reject) => {
		uni[typeName]({
			url,
			success(res) {
				resolve(res)
			},
			fail(err) {
				reject(err)
			}
		})
	})
}