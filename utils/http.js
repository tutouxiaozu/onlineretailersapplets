

// import { baseUrl, whiteList } from "./config";
import result from "./config";
import { formetUrl } from "./utils";

export const http = function ({ timeout, baseUrl, errorHandle, requestHandle }) {
    const request = async (url, method = "get", data = {}, header = {}) => {
        // 定义请求对象
        const requestObj = {
            originUrl: url,
            baseUrl,
            url: `${baseUrl}${url}`,
            method,
            data,
            header,
            timeout
        };

        const config = requestHandle && requestHandle(requestObj) || requestObj;
        // 判断请求对象是否是 合法 对象;
        if (typeof config === "object" && config.url && config.method) {
            try {
                const res = await uni.request(config);
                // 判断res是否是数组,  长度dayu0
                if (Array.isArray(res) && res.length && res[1]) {
                    return res[1].data ? res[1].data : res[1];
                }
                return Promise.reject(res);
            } catch (error) {
                // 返回一个错误状态;
                errorHandle && errorHandle(error);
                return Promise.reject(error);
            }
        }

    }

    return {
        post(url, data, header = {}) {
            return request(url, 'POST', data, header)
        },
        get(url, query = {}, header = {}) {
            // 格式化 请求地址;
            const formetendUrl = formetUrl(url, query);
            return request(formetendUrl, 'GET', {}, header)
        },
        put(url, data, header = {}) {
            return request(url, 'PUT', data, header)
        },
        delete(url, params, header = {}) {
            console.log(params, 'params');
            if (params) {
                return request(`${url}`, 'DELETE', params.params, header)
            }
            return request(`${url}`, 'DELETE', {}, header)
        }
    }
}

const request = http({
    timeout: 20000,
    baseUrl: result.baseUrl,
    requestHandle: config => {
        // 设置请求拦截
        // 设置 authorization 设置token,鉴权;
        const header = {
            "Authorization": "Bearer " + uni.getStorageSync('token')
        }
        // 判断请求地址 是否在白名单中
        if (result.whiteList.includes(config.originUrl)) {
            delete header.Authorization
        }
        return {
            ...config,
            header
        }
    },
    errorHandle: error => { //错误处理
        console.log(error);
    }
})

export default request;


// export default function request(options) {
//     // options为调用时传入的参数对象
//     return new Promise((resolve, reject) => {
//         wx.request({
//             header: {
//                 "pc-token": "4a82b23dbbf3b23fd8aa291076e660ec",
//                 //定义公共头部信息
//             },
//             url: baseUrl + options.url,
//             // 拼接请求地址
//             data: options.data || {},
//             // 传入data参数
//             method: options.method || "get",
//             // 传入请求类型默认为get
//             success: function (res) {
//                 // 成功回调
//                 resolve(res);
//             },
//             fail: function (res) {
//                 // 失败回调
//                 reject(res);
//             },
//         });
//     });
// }
